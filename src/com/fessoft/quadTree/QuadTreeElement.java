package com.fessoft.quadTree;

import java.util.ArrayList;
import java.util.List;

public class QuadTreeElement<T> {
	
	private volatile QuadTreeNode parent = null;
	
	private volatile int x;
	private volatile int y;
	
	private final T attribute;
	
	public QuadTreeElement() {
		this.attribute = (T)this;
	}
	
	public QuadTreeElement(T attribute) {
		this.attribute = attribute;
	}
		
	protected void set(int x, int y, QuadTreeNode parent) {
		this.x = x;
		this.y = y;
		this.parent = parent;
	}
	
	protected void set(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	protected void setParent(QuadTreeNode parent) {
		this.parent = parent;
	}
		
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public T getAttribute() {
		return attribute;
	}
	
	protected QuadTreeNode getParent() {
		return parent;
	}
	
	public List<QuadTreeElement> getNeighbours(int radiusX, int radiusY) {
		synchronized(this) {
			int x = getX();
			int y = getY();
		}
		
		int minX = x - radiusX;
		int maxX = x + radiusX;
		int minY = y - radiusY;
		int maxY = y + radiusY;
		
		QuadTreeNode node = getNodeThatContainsFully(minX, minY, maxX, maxY);		
		ArrayList<QuadTreeElement> results = new ArrayList<QuadTreeElement>();
		node.lockForReading();
		node.__query(minX, minY, maxX, maxY, results);
		
		AdaptiveQuadTreeNode.checkLocks();
		return results;
	}
	
	private QuadTreeNode getNodeThatContainsFully(int minX, int minY, int maxX, int maxY) {
		QuadTreeNode node = getParent();
		
		while(true) {
			if(node.containsFully(minX, minY, maxX, maxY)){
				return node;
			}
			
			if(node.parent == null) {
				return node;
			}else{
				node = node.parent;
			}
		}
	}
	
	public void move(int x, int y) {
		QuadTreeNode node = getParent();
		
		// This element is not part of a tree
		if(node == null) {
			this.x = x;
			this.y = y;
			AdaptiveQuadTreeNode.checkLocks();
			return;
		}
		
		// Quick path: Position change within the same node
		if(node.containsFully(x, y, x, y)){
			node.lockForReading();
			if(node == getParent()) {
				this.x=x;
				this.y=y;
				node.unlockForReading();
				AdaptiveQuadTreeNode.checkLocks();
				return;
			}
			node.unlockForReading();
		}
		
		// Slow path: Position change that may span multiple nodes
		do {
			node.lockForWriting();
			if(node.containsFully(x, y, x, y)){
				break;
			}
			node.unlockForWriting();
			
			node = node.parent;
		} while(node != null);
		
		node.update(this, x, y);
		AdaptiveQuadTreeNode.checkLocks();
	}
	
	public void remove() {
		QuadTreeNode parent;
		
		while(true){
			parent = getParent();
			
			if(parent == null)
				return;
			
			parent.lockForWriting();
			if(parent == getParent()) {
				parent.remove(this);
				return;
			}
			parent.unlockForWriting();
		}
	}
		
	public String toString() {
		return "("+x+", "+y+")";
	}

}
