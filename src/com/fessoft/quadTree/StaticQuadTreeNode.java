package com.fessoft.quadTree;

import java.util.ArrayList;
import java.util.HashSet;

public class StaticQuadTreeNode extends QuadTreeNode {

	private final QuadTreeNode[] nodes;
	
	protected StaticQuadTreeNode(QuadTreeNode parent, int x1, int y1, int x2, int y2, int staticDepth) {
		super(parent, x1, y1, x2, y2);
		
		nodes = new QuadTreeNode[4];
		
		if(staticDepth == 1) {
			nodes[0] = new AdaptiveQuadTreeNode(this, minX, 	minY, 		pivotX, 	pivotY);
			nodes[1] = new AdaptiveQuadTreeNode(this, pivotX, 	minY, 		maxX, 		pivotY);
			nodes[2] = new AdaptiveQuadTreeNode(this, minX, 	pivotY, 	pivotX, 	maxY);
			nodes[3] = new AdaptiveQuadTreeNode(this, pivotX, 	pivotY, 	maxX, 		maxY);
		}else{
			nodes[0] = new StaticQuadTreeNode(this, minX, 	minY, 		pivotX, 	pivotY, staticDepth-1);
			nodes[1] = new StaticQuadTreeNode(this, pivotX, minY, 		maxX, 		pivotY, staticDepth-1);
			nodes[2] = new StaticQuadTreeNode(this, minX, 	pivotY, 	pivotX, 	maxY, 	staticDepth-1);
			nodes[3] = new StaticQuadTreeNode(this, pivotX, pivotY, 	maxX, 		maxY, 	staticDepth-1);
		}
	}
	
	@Override
	protected void __insert(QuadTreeElement element) {
		QuadTreeNode node;
		if(element.getY() < pivotY) {
			if(element.getX() < pivotX) {
				node = nodes[0];
			}else{
				node = nodes[1];
			}
		}else{
			if(element.getX() < pivotX) {
				node = nodes[2];
			}else{
				node = nodes[3];
			}
		}
		
		node.lockForWriting();
		node.__insert(element);
	}

	@Override
	protected void remove(QuadTreeElement element) {
		throw new RuntimeException("NOT IMPLEMENTED");
	}

	@Override
	protected void update(QuadTreeElement element, int newX, int newY) {
		int x,y;
		synchronized(element) {
			x = element.getX();
			y = element.getY();
		}
		
		int[][] positions = new int[2][2];
		positions[0][0] = x;
		positions[0][1] = y;
		positions[1][0] = newX;
		positions[1][1] = newY;
		
		ArrayList<QuadTreeNode> locked = new ArrayList<QuadTreeNode>();
		deepSelectiveWriteLock(positions, locked);
					
		element.remove();
		element.set(newX, newY);
		__insert(element);
		
		for(QuadTreeNode lockedNode : locked) {
			lockedNode.unlockForWriting();
		}

	}

	@Override
	protected void maybeMerge() {}

	@Override
	protected void __query(int minX, int minY, int maxX, int maxY,ArrayList<QuadTreeElement> elementsFound) {
		if(!(minX > this.maxX || maxX < this.minX || minY > this.maxY || maxY < this.minY)) {
			QuadTreeNode[] nodes = this.nodes;
			
			for(QuadTreeNode node : nodes) {
				node.lockForReading();
			}
			
			this.unlockForReading();
			
			for(QuadTreeNode node : nodes) {
				node.__query(minX, minY, maxX, maxY, elementsFound);
			}
		}

	}

	@Override
	protected void getAllElements(HashSet<QuadTreeElement> elements) {
		for(QuadTreeNode node : this.nodes) {
			node.getAllElements(elements);
		}
	}

	@Override
	protected int __hasCountOf(int count) {
		int totalCount = 0;
		for(QuadTreeNode node : nodes) {
			totalCount += node.__hasCountOf(count);
			
			if(totalCount >= count) {
				return totalCount;
			}
		}
		return totalCount;
	}

	protected void lockForReading() {}
	protected void unlockForReading() {}
	protected void lockForWriting() {}
	protected void unlockForWriting() {}

	@Override
	protected void deepWriteLock() {
		for(QuadTreeNode node : nodes) {
			node.deepWriteLock();
		}
	}

	@Override
	protected void deepWriteUnLock() {
		for(QuadTreeNode node : nodes) {
			node.deepWriteUnLock();
		}
	}

	protected void deepSelectiveWriteLock(int[][] positions, ArrayList<QuadTreeNode> locked) {
		boolean positionMatch = false;
		for(int[] position :  positions) {
			if(this.containsFully(position[0], position[1], position[0], position[1])) {
				positionMatch = true;
				break;
			}
		}
		
		if(positionMatch) {
			lockForWriting();
			locked.add(this);
			for(QuadTreeNode node : nodes) {
				node.deepSelectiveWriteLock(positions, locked);
			}
		}
	}

	public void printDebugData(String ident) {
		System.out.println(ident+""+this);
		for(QuadTreeNode node : nodes) {
			node.printDebugData(ident+"\t");
		}			
	}

}
