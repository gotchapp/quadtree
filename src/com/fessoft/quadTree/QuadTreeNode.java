package com.fessoft.quadTree;

import java.util.ArrayList;
import java.util.HashSet;

public abstract class QuadTreeNode {
	protected final int minX;
	protected final int maxX;
	protected final int minY;
	protected final int maxY;
	
	protected final int pivotX;
	protected final int pivotY;
	
	protected final QuadTreeNode parent;
	
	protected QuadTreeNode(QuadTreeNode parent, int x1, int y1, int x2, int y2) {
		this.parent = parent;
		
		minX = Math.min(x1, x2);
		maxX = Math.max(x1, x2);
		minY = Math.min(y1, y2);
		maxY = Math.max(y1, y2);
		
		this.pivotX = (minX + maxX)/2;
		this.pivotY = (minY + maxY)/2;
	}
	
	protected boolean containsFully(int minX, int minY, int maxX, int maxY) {
		return minX >= this.minX && minY >= this.minY && maxX <= this.maxX && maxY <= this.maxY;
	}
	
	protected abstract void __insert(QuadTreeElement element);
	protected abstract void remove(QuadTreeElement element);
	protected abstract void update(QuadTreeElement element, int newX, int newY);
	protected abstract void maybeMerge();
	
	protected abstract void __query(int minX, int minY, int maxX, int maxY, ArrayList<QuadTreeElement> elementsFound);
	protected abstract void getAllElements(HashSet<QuadTreeElement> elements);
	protected abstract int __hasCountOf(int count);
	
	protected abstract void lockForReading();
	protected abstract void unlockForReading();
	protected abstract void lockForWriting();
	protected abstract void unlockForWriting();
	
	protected abstract void deepWriteLock();
	protected abstract void deepWriteUnLock();
	protected abstract void deepSelectiveWriteLock(int[][] positions, ArrayList<QuadTreeNode> locked);
	protected abstract void printDebugData(String ident);
}
