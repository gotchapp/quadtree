package com.fessoft.quadTree;

import java.util.ArrayList;
import java.util.List;

public class QuadTree extends StaticQuadTreeNode {

	protected static final boolean DEBUG = false;
	
	public QuadTree() {
		super(null,-90000000,-180000000,90000000, 180000000,5);
	}
	
	public QuadTree(int x1, int y1, int x2, int y2) {
		super(null, x1, y1, x2, y2, 5);
	}
	
	public List<QuadTreeElement> query(int x1, int y1, int x2, int y2) {		
		int minX = Math.min(x1, x2);
		int maxX = Math.max(x1, x2);
		int minY = Math.min(y1, y2);
		int maxY = Math.max(y1, y2);
		
		ArrayList<QuadTreeElement> elements = new ArrayList<QuadTreeElement>();
		
		this.lockForReading();
		__query(minX, minY, maxX, maxY, elements);
		
		AdaptiveQuadTreeNode.checkLocks();
		return elements;
	}
	
	public void insert(QuadTreeElement element) {
		this.lockForWriting();
		this.__insert(element);
		AdaptiveQuadTreeNode.checkLocks();
	}
}
