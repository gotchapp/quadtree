package com.fessoft.quadTree.stressTest;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import com.fessoft.quadTree.QuadTreeElement;

public class StressTestElement extends QuadTreeElement {
		
	public StressTestElement() {
		super(null);
	}
	
	public void randomPosition(ThreadLocalRandom r) {	
		int x = r.nextInt(180000000)-90000000;
		int y = r.nextInt(360000000)-180000000;
		
		this.move(x, y);
	}
	
	public void moveABit(ThreadLocalRandom r) {
		int x, y;
		
		synchronized(this) {
			x = this.getX() + r.nextInt(100000) - 50000;
			y = this.getY() + r.nextInt(100000) - 50000;
		}
		
		if(x < 90000000 && x > -90000000 && y < 180000000 && y > -180000000) {
			this.move(x, y);
		}
	}
	
	public void doRemove() {
		this.remove();
	}
}